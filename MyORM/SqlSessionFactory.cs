﻿using System.Configuration;

namespace MyORM
{
    public class SqlSessionFactory
    {
        public static ISqlSesstion cretateSession()
        {
            return new DefaultSqlSession(ConfigurationManager.ConnectionStrings["sqlConn"].ToString());
        }
    }
}
