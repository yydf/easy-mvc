﻿using MyORM.mapper;
using MyUtils;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace MyORM
{
    public interface ISqlSesstion
    {
        bool SaveOrUpdate(object obj);

        bool Execute(string sql,params object[] objs);

        T Single<T>(string sql, params object[] objs);

        List<T> Query<T>(string sql, params object[] objs);

        bool Run(string procName, List<SqlParameter> paras);

        object GetSingle(string sql, params object[] objs);

        SqlTransaction beginTransaction();

        bool SaveOrUpdate(SqlTransaction tran, object entity);

        bool Execute(SqlTransaction tran, string sql, params object[] objs);

        void close(SqlTransaction tran);

        Map QueryMap(string sql, params object[] objs);
    }
}
