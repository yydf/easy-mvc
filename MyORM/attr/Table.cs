﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyORM.attr
{
    [AttributeUsage(AttributeTargets.Class)]
    public class Table : Attribute
    {
        public Table(string tableName)
        {
            this.tableName = tableName;
        }

        public string tableName { get; set; }
    }
}
