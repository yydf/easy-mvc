﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyORM.attr
{
    [AttributeUsage(AttributeTargets.Property)]
    public class Column : Attribute
    {
        public Column(string columnName)
        {
            this.columnName = columnName;
        }

        public string columnName { get; set; }
    }
}
