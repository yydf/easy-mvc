﻿using Newtonsoft.Json;

namespace MyUtils
{
    public class JSONHelper
    {
        public static string serialize(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
    }
}
