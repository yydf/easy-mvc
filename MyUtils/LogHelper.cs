using System;
using log4net;

namespace MyUtils
{
    /// <summary>
    /// 日志管理类
    /// </summary>
    public class LogHelper
    {
        private static bool log4netIsConfiged;
        static readonly ILog defaultLogger = GetDefaultLogger();

        /// <summary>
        /// 获得默认的日志记录器
        /// </summary>
        /// <returns></returns>
        public static ILog GetDefaultLogger()
        {
            TryConfig();
            return LogManager.GetLogger("默认");
        }

        public static void Info(string message)
        {
            defaultLogger.Info(message);
        }

        public static void Error(Exception ex)
        {
            defaultLogger.Error(ex.Message, ex);
        }

        public static void Error(string errorMsg, Exception ex)
        {
            defaultLogger.Error(errorMsg, ex);
        }

        private static void TryConfig()
        {
            if (log4netIsConfiged == false)
            {
                try
                {
                    log4net.Config.XmlConfigurator.Configure();
                    log4netIsConfiged = true;
                }
                catch
                {
                }
            }
        }
    }
}