﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyUtils
{
    public class StringUtils
    {
        public static string NullToBlank(object obj)
        {
            if (obj == null)
                return string.Empty;
            return obj.ToString();
        }
    }
}
