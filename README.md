# easy-mvc
可在webForm上使用的mvc框架，无需导入.net mvc的大量引用包，同时支持web和json开发。

### 特色
* 自动扫描所有Controller和Interceptor
* 自动注册所有扫描到的url链接
* 根据~自动解析为从根目录开始，不受类的路径影响
* 根据[AutoWired]自动扫描实现类，无需指定实现类
* 自动获取表单信息并转换为Entity对象
* 自动加载Vo对象到表单
* 安全的SQL处理方式，防止注入等操作

## UI效果图
![欢迎页](https://gitee.com/yydf/easy-mvc/raw/master/screenshot/1.png "欢迎页")
![管理员操作](https://gitee.com/yydf/easy-mvc/raw/master/screenshot/2.png "管理员操作")

### 项目结构
* MyMVC 自定义MVC实现包
* MyORM 自定义数据库操作实现包
* MyUtils 自定义工具类实现包
* WebSite-Controllers 所有控制器类(加载数据、页面跳转等)
* WebSite-DAL 数据库操作实现
* WebSite-Entity 与数据库结构对应的对象
* WebSite-IDAL 数据库操作接口，数据库更换可通过实现该接口处理
* WebSite-Interceptors 用来拦截所有页面处理的拦截器，例如登录
* WebSite-Models 所有页面数据model
* WebSite-View 所有aspx页面和资源

### 运行条件
* 打开解决方案后可直接运行

### ChangeLog
<a href='https://git.oschina.net/yydf/easy-mvc/releases'>点击查看更新日志</a>

### Discussing
* <a href='https://git.oschina.net/yydf/easy-mvc/issues/new'>提交issue</a>
* 交流群616698275 答案easy
* email：441430565@qq.com

### WIKI
https://gitee.com/yydf/easy-mvc/wikis/pages