﻿using System.Reflection;
using System.Web;

namespace MyMVC.handler
{
    public class VoidHandler : BaseHandler
    {
        public VoidHandler(HttpContext context, MethodInfo method, ActionSupport baseType)
        {
            this.method = method;
            this.baseType = baseType;
        }

        public override void process(HttpContext context)
        {
            method.Invoke(baseType, null);
            context.Response.End();
        }
    }
}
