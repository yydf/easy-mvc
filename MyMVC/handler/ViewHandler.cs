﻿using MyMVC.mapper;
using System.Collections.Generic;
using System.Reflection;
using System.Web;

namespace MyMVC.handler
{
    public class ViewHandler : BaseHandler
    {
        public ViewHandler(HttpContext context, MethodInfo method, ActionSupport baseType)
        {
            this.method = method;
            this.baseType = baseType;
        }

        public override void process(HttpContext context)
        {
            ModelAndView mav = (ModelAndView)method.Invoke(baseType, null);
            Dictionary<string, object> dict = mav.getParameters();
            foreach (string key in dict.Keys)
            {
                context.Items.Add(key, dict[key]);
            }
            if (mav.viewName.EndsWith(".html"))
                context.Server.Transfer("~/" + mav.viewName, true);
            else
                context.Server.Transfer("~/" + mav.viewName + ".aspx", true);
            context.Response.End();
        }
    }
}
