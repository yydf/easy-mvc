﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web;
using System.Web.SessionState;

namespace MyMVC.handler
{
    public abstract class BaseHandler : IHttpHandler, IRequiresSessionState
    {
        protected MethodInfo method;
        protected ActionSupport baseType;

        public bool a(HttpContext context)
        {
            List<Type> interceptors = ContextLoader.getInterceptors();
            AbstractActionInterceptor interceptor;
            foreach (Type t in interceptors)
            {
                interceptor = (AbstractActionInterceptor)Activator.CreateInstance(t);
                if (!interceptor.intercept(context.Session, context.Request, context.Response))
                    return false;
            }
            return true;
        }

        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            if (a(context) && method != null)
                process(context);
        }

        public abstract void process(HttpContext context);
    }
}
