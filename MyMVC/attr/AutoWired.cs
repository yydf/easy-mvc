﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyMVC.attr
{
    [AttributeUsage(AttributeTargets.Field)]
    public class AutoWired : Attribute
    {
    }
}
