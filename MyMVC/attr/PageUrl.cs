﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyMVC.attr
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class PageUrl : Attribute
    {
        public PageUrl(string url)
        {
            this.Url = url;
        }

        public string Url
        {
            get;
            set;
        }
    }
}
