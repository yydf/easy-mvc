﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyMVC.mapper
{
    public class JSON
    {
        Dictionary<string, object> dict;

        public JSON()
        {
            dict = new Dictionary<string, object>();
        }

        public void Add(string key, object obj)
        {
            dict.Add(key, obj);
        }

        internal Dictionary<string, object> getData()
        {
            return dict;
        }
    }
}
