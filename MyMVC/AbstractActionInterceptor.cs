﻿using System.Web;
using System.Web.SessionState;

namespace MyMVC
{
    public abstract class AbstractActionInterceptor
    {
        public abstract bool intercept(HttpSessionState session, HttpRequest arg0, HttpResponse arg1);
    }
}
