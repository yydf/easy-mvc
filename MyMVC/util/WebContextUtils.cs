﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyMVC.util
{
    class WebContextUtils
    {
        internal static bool isGZip(System.Web.HttpRequest request)
        {
            bool flag = false;
            string encoding = request.Headers["Accept-Encoding"];
            if (encoding != null && encoding.IndexOf("gzip") > -1)
                flag = true;
            return flag;
        }
    }
}
