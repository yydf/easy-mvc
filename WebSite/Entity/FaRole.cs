﻿using MyORM.attr;
using System;

namespace WebSite.Entity
{
    [Table("fa_role")]
    public class FaRole
    {
        [Id("role_id")]
        [Column("role_id")]
        public int? RoleId { get; set; }

        [Column("role_addtime")]
        public DateTime? RoleAddtime { get; set; }

        [Column("role_name")]
        public string RoleName { get; set; }

        [Column("role_desc")]
        public string RoleDesc { get; set; }
    }
}