﻿using MyORM.attr;
using System;

namespace WebSite.Entity
{
    [Table("fa_manager")]
    public class FaManager
    {
        [Column("mgr_name")]
        public string MgrName { get; set; }

        [Column("mgr_mobile")]
        public string MgrMobile { get; set; }

        [Column("mgr_remark")]
        public string MgrRemark { get; set; }

        [Column("mgr_pwd")]
        public string MgrPwd { get; set; }

        [Column("role_id")]
        public int RoleId { get; set; }

        [Column("mgr_sex")]
        public int MgrSex { get; set; }

        [Id("mgr_id")]
        [Column("mgr_id")]
        public int? MgrId { get; set; }

        [Column("mgr_addtime")]
        public DateTime? MgrAddtime { get; set; }
    }
}