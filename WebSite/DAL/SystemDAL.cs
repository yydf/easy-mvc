﻿using MyUtils;
using System.Collections.Generic;
using WebSite.IDAL;
using WebSite.Models;

namespace WebSite.DAL
{
    public class SystemDAL : BaseDAL, ISystem
    {
        public List<LogListVo> getLogList()
        {
            string sql = "select t.*,t1.mgr_name FROM fa_manager_log t, fa_manager t1 where t.mgr_id=t1.mgr_id order by t.log_addtime DESC";
            return getDBUtils().Query<LogListVo>(sql);
        }

        public SiteVo getSiteConfig()
        {
            SiteVo vo = new SiteVo();
            Map map = getDBUtils().QueryMap("select * from fa_config");
            if (map != null) {
                vo.SiteName = map.getString("site_name");
                vo.CopyRight = map.getString("copy_right");
                vo.ICP = map.getString("icp");
                vo.StatJS = map.getString("stat_js");
            }
            return vo;
        }

        public bool editConfig(SiteVo site)
        {
            getDBUtils().Execute("update fa_config set config_value=@0 where config_key='site_name'", site.SiteName);
            getDBUtils().Execute("update fa_config set config_value=@0 where config_key='copy_right'", site.CopyRight);
            getDBUtils().Execute("update fa_config set config_value=@0 where config_key='icp'", site.ICP);
            getDBUtils().Execute("update fa_config set config_value=@0 where config_key='stat_js'", site.StatJS);
            return true;
        }

        public bool delLog(string logId)
        {
            string sql = "delete FROM fa_manager_log where log_id = @0";
            return getDBUtils().Execute(sql, logId);
        }

        public List<LogListVo> searchLog(string minDate, string maxDate)
        {
            string sql = "select t.*,t1.mgr_name FROM fa_manager_log t, fa_manager t1 where t.mgr_id=t1.mgr_id and t.log_addtime between @0 and @1 order by t.log_addtime DESC";
            return getDBUtils().Query<LogListVo>(sql, minDate, maxDate);
        }

        public List<FMoudleListVo> getFModuleList()
        {
            string sql = "select * FROM fa_module where is_enable=1";
            List<MoudleListVo> moduleList = getDBUtils().Query<MoudleListVo>(sql);
            return convertModule(moduleList);
        }

        private List<FMoudleListVo> convertModule(List<MoudleListVo> moduleList)
        {
            List<FMoudleListVo> fmodules = new List<FMoudleListVo>();
            Dictionary<string, FMoudleListVo> fmoduleNames = new Dictionary<string, FMoudleListVo>();
            foreach (MoudleListVo m in moduleList)
            {
                if (fmoduleNames.ContainsKey(m.ModuleFname))
                {
                    FMoudleListVo vo = fmoduleNames[m.ModuleFname];
                    vo.Modules.Add(m);
                }
                else
                {
                    FMoudleListVo vo = new FMoudleListVo();
                    vo.ModuleFname = m.ModuleFname;
                    vo.ModuleFicon = m.ModuleFicon;
                    vo.Modules = new List<MoudleListVo>();
                    vo.Modules.Add(m);
                    fmodules.Add(vo);
                    fmoduleNames.Add(m.ModuleFname, vo);
                }
            }
            return fmodules;
        }


        public List<FMoudleListVo> getFModuleList(string roleId, bool all)
        {
            string sql;
            if (all)
                sql = "select t.*, ISNULL(t1.is_enable,0) checked FROM fa_module t LEFT JOIN (select COUNT(1) is_enable,module_id FROM fa_role_module WHERE role_id=@0 group by module_id) t1 ON t.module_id = t1.module_id where t.is_enable=1";
            else
            {
                if("1".Equals(roleId))
                    sql = "select * FROM fa_module where is_enable=1";
                else
                    sql = "select * FROM fa_module where module_id in (select module_id FROM fa_role_module WHERE role_id=@0) and is_enable=1";
            }
            List<MoudleListVo> moduleList = getDBUtils().Query<MoudleListVo>(sql, roleId);
            return convertModule(moduleList);
        }


        public int getLoginNum(string mgrId)
        {
            string sql = "select count(1) from fa_manager_log where mgr_id=@0";
            return (int)getDBUtils().GetSingle(sql, mgrId);
        }


        public LogListVo getLastLog(string mgrId)
        {
            string sql = "select top 2 * from fa_manager_log where mgr_id=@0 order by log_addtime DESC";
            List<LogListVo> logs = getDBUtils().Query<LogListVo>(sql, mgrId);
            if (logs != null && logs.Count == 2)
                return logs[1];
            return null;
        }
    }
}