﻿using MyUtils;
using System;
using System.Collections.Generic;
using WebSite.Entity;
using WebSite.IDAL;
using WebSite.Models;

namespace WebSite.DAL
{
    public class ManagerDAL : BaseDAL, IManager
    {
        public List<ManagerListVo> getManagerList(string keyWord)
        {
            if(string.IsNullOrEmpty(keyWord))
                return getDBUtils().Query<ManagerListVo>("select t.*,t1.role_name from fa_manager t,fa_role t1 where t.role_id=t1.role_id");
            keyWord = "%" + keyWord + "%";
            return getDBUtils().Query<ManagerListVo>("select t.*,t1.role_name from fa_manager t,fa_role t1 where t.role_id=t1.role_id AND t.mgr_name like @0", keyWord);
        }

        public bool addManager(FaManager mgr)
        {
            if (mgr.MgrId == null)
            {
                mgr.MgrAddtime = DateTime.Now;
                mgr.MgrPwd = MD5Utils.GetMD5(mgr.MgrPwd);
            }
            return getDBUtils().SaveOrUpdate(mgr);
        }

        public bool delManager(string mgrId)
        {
            string sql = "delete from fa_manager where mgr_id=@0";
            return getDBUtils().Execute(sql, mgrId);
        }


        public ManagerVo getManagerVo(string mgrId)
        {
            string sql = "select * from fa_manager where mgr_id=@0";
            return getDBUtils().Single<ManagerVo>(sql, mgrId);
        }


        public bool changeFlag(string mgrId, string status)
        {
            string sql = "update fa_manager set mgr_status =@0 where mgr_id=@1";
            return getDBUtils().Execute(sql, status, mgrId);
        }
    }
}