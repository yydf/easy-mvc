﻿using MyORM;

namespace WebSite.DAL
{
    public class BaseDAL
    {
        ISqlSesstion session = null;
        protected ISqlSesstion getDBUtils()
        {
            if (session == null)
                session = SqlSessionFactory.cretateSession();
            return session;
        }
    }
}