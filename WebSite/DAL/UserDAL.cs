﻿using MyUtils;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WebSite.IDAL;

namespace WebSite.DAL
{
    public class UserDAL : BaseDAL, IUser
    {
        public Map checkLogin(string userName, string pwd, string ip)
        {
            List<SqlParameter> paras = new List<SqlParameter>();
            paras.Add(new SqlParameter("@userName", userName));
            paras.Add(new SqlParameter("@pwd", pwd));
            paras.Add(new SqlParameter("@ip", ip));
            SqlParameter para = new SqlParameter("@errId", SqlDbType.Int);
            para.Direction = System.Data.ParameterDirection.Output;
            paras.Add(para);
            SqlParameter para1 = new SqlParameter("@mgrId", SqlDbType.Int);
            para1.Direction = ParameterDirection.Output;
            paras.Add(para1);
            SqlParameter para2 = new SqlParameter("@roleId", SqlDbType.Int);
            para2.Direction = ParameterDirection.Output;
            paras.Add(para2);
            Map map = new Map();
            if (getDBUtils().Run("proc_userLogin", paras))
            {
                map.Add("errId", para.Value);
                map.Add("mgrId", para1.Value);
                map.Add("roleId", para2.Value);
                return map;
            }
            map.Add("errId", 999000);
            return map;
        }
    }
}