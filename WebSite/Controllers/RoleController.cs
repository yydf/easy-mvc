﻿using MyMVC.attr;
using MyMVC.mapper;
using WebSite.Entity;
using WebSite.IDAL;

namespace WebSite.Controllers
{
    [PageUrl("/role")]
    public class RoleController : BaseController
    {
        [AutoWired]
        private IRole role = null;

        [AutoWired]
        private ISystem system = null;

        [PageUrl("/toRoleList")]
        public ModelAndView toRoleList()
        {
            ModelAndView mav = new ModelAndView("/View/role_list");
            mav.addObject("dataList", role.getRoleList(), "size");
            return mav;
        }

        [PageUrl("/toAddRole")]
        public ModelAndView toAddRole()
        {
            string roleId = getParameter("a");
            ModelAndView mav = new ModelAndView("/View/role_add");
            if (!string.IsNullOrEmpty(roleId))
            {
                mav.addObject(role.getRoleVo(roleId));
                mav.addObject("moduleList", system.getFModuleList(roleId, true));
            }
            else
            {
                mav.addObject("moduleList", system.getFModuleList());
            }
            return mav;
        }

        [PageUrl("/addRole")]
        public JSON addRole()
        {
            FaRole entity = getPostData<FaRole>();
            string menuStr = getParameter("menuStr");
            if (role.addRole(entity, menuStr))
                return getOK();
            return getError();
        }

        [PageUrl("/delRole")]
        public JSON delRole()
        {
            string roleId = getParameter("a");
            if (role.delRole(roleId))
                return getOK();
            return getError();
        }
    }
}