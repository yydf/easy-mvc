﻿using MyMVC.attr;
using MyMVC.mapper;
using System;
using WebSite.IDAL;
using WebSite.Models;

namespace WebSite.Controllers
{
    [PageUrl("/system")]
    public class SystemController : BaseController
    {
        [AutoWired]
        private ISystem system = null;

        [PageUrl("/toLogList")]
        public ModelAndView toManagerList()
        {
            ModelAndView mav = new ModelAndView("/View/log_list");
            mav.addObject("dataList", system.getLogList());
            return mav;
        }

        [PageUrl("/toConfig")]
        public ModelAndView toConfig()
        {
            ModelAndView mav = new ModelAndView("/View/config");
            mav.addObject(system.getSiteConfig());
            return mav;
        }

        [PageUrl("/editConfig")]
        public JSON editConfig()
        {
            SiteVo site = getPostData<SiteVo>();
            if (system.editConfig(site))
                return getOK();
            return getError();
        }

        [PageUrl("/searchLog")]
        public ModelAndView searchLog()
        {
            string minDate = getParameter("a");
            if (string.IsNullOrEmpty(minDate))
                minDate = "2018-01-01";
            minDate = minDate + " 00:00:00";
            string maxDate = getParameter("b");
            if (string.IsNullOrEmpty(maxDate))
                maxDate = DateTime.Now.ToString("yyyy-MM-dd");
            maxDate = maxDate + " 23:59:59";
            ModelAndView mav = new ModelAndView("/View/log_list");
            mav.addObject("dataList", system.searchLog(minDate, maxDate));
            mav.addObject("logmin", minDate);
            mav.addObject("logmax", maxDate);
            return mav;
        }

        [PageUrl("/delLog")]
        public JSON delLog()
        {
            string logId = getParameter("a");
            if (system.delLog(logId))
                return getOK();
            return getError();
        }
    }
}