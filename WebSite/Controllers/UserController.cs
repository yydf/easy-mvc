﻿using MyMVC.attr;
using MyMVC.mapper;
using MyUtils;
using WebSite.IDAL;

namespace WebSite.Controllers
{
    [PageUrl("/user")]
    public class UserController : BaseController
    {
        [AutoWired]
        private IUser user = null;

        [PageUrl("/toLogin")]
        public JSON toLogin()
        {
            string userName = getParameter("username");
            string pwd = getParameter("pwd");
            pwd = MD5Utils.GetMD5(pwd).ToLower();
            Map map = user.checkLogin(userName, pwd, getIP());
            int errId = map.getInt("errId");
            if (errId == 0)
            {
                setSession("mgrId", map.getInt("mgrId"));
                setSession("roleId", map.getInt("roleId"));
                return getOK("url", "/toMain");
            }
            return getError(errId);
        }

        [PageUrl("/logout")]
        public void logout()
        {
            getSession().Clear();
            getResponse().Redirect("/");
        }
    }
}