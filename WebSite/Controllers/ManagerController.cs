﻿using MyMVC.attr;
using MyMVC.mapper;
using WebSite.Entity;
using WebSite.IDAL;

namespace WebSite.Controllers
{
    [PageUrl("/manager")]
    public class ManagerController : BaseController
    {
        [AutoWired]
        private IManager manager = null;

        [AutoWired]
        private IRole role = null;

        [PageUrl("/toManagerList")]
        public ModelAndView toManagerList()
        {
            string keyWord = getParameter("a");
            ModelAndView mav = new ModelAndView("/View/admin_list");
            mav.addObject("dataList", manager.getManagerList(keyWord), "size");
            mav.addObject("keyWord", keyWord);
            return mav;
        }

        [PageUrl("/toAddManager")]
        public ModelAndView toAddManager()
        {
            string mgrId = getParameter("a");
            ModelAndView mav = new ModelAndView("/View/admin_add");
            mav.addObject("roleList", role.getRoleList());
            mav.addObject("MgrSex", 1);
            if (!string.IsNullOrEmpty(mgrId))
            {
                mav.addObject(manager.getManagerVo(mgrId));
            }
            return mav;
        }

        [PageUrl("/addManager")]
        public JSON addManager()
        {
            FaManager mgr = getPostData<FaManager>();
            if (manager.addManager(mgr))
                return getOK();
            return getError();
        }

        [PageUrl("/delManager")]
        public JSON delManager()
        {
            string mgrId = getParameter("a");
            if (manager.delManager(mgrId))
                return getOK();
            return getError();
        }

        [PageUrl("/changeFlag")]
        public JSON changeFlag()
        {
            string mgrId = getParameter("a");
            string status = getParameter("s");
            if (manager.changeFlag(mgrId, status))
                return getOK();
            return getError();
        }
    }
}