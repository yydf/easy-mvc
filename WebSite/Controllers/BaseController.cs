﻿using MyMVC;
using MyMVC.attr;
using MyMVC.mapper;
using System;
using WebSite.IDAL;
using WebSite.Models;

namespace WebSite.Controllers
{

    public class BaseController : ActionSupport
    {
        [AutoWired]
        private IRole role = null;

        [AutoWired]
        private IManager manager = null;

        [AutoWired]
        private ISystem system = null;


        [PageUrl("/")]
        public ModelAndView toIndex()
        {
            ModelAndView mav = new ModelAndView("/login.html");
            return mav;
        }

        [PageUrl("/toMain")]
        public ModelAndView toMain()
        {
            ModelAndView mav = new ModelAndView("/View/main");
            string roleId = getSession("roleId").ToString();
            string mgrId = getSession("mgrId").ToString();
            mav.addObject(system.getSiteConfig());
            mav.addObject("RoleName", role.getRoleName(roleId));
            ManagerVo mgrVo = manager.getManagerVo(mgrId);
            mav.addObject("MgrName", mgrVo.MgrName);
            mav.addObject("moduleList", system.getFModuleList(roleId, false));
            return mav;
        }

        [PageUrl("/toWelcome")]
        public ModelAndView toWelcome()
        {
            ModelAndView mav = new ModelAndView("/View/welcome");
            string mgrId = getSession("mgrId").ToString();
            mav.addObject(system.getSiteConfig());
            mav.addObject("loginNum", system.getLoginNum(mgrId));
            mav.addObject("logVo", system.getLastLog(mgrId));
            mav.addObject("sessionId", getSession().SessionID);
            mav.addObject("time", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            mav.addObject("server", System.Net.Dns.GetHostName());
            mav.addObject("version", Environment.Version);
            return mav;
        }

        protected JSON getOK(params object[] objs)
        {
            JSON json = new JSON();
            json.Add("success", true);
            string key = "";
            for (int i = 0; i < objs.Length; i++)
            {
                if (i % 2 == 0)
                {
                    key = (string)objs[i];
                }
                else
                {
                    json.Add(key, objs[i]);
                }
            }
            return json;
        }

        protected JSON getError()
        {
            JSON json = new JSON();
            json.Add("success", false);
            json.Add("errmsg", "系统错误");
            return json;
        }

        protected JSON getError(int errId)
        {
            JSON json = new JSON();
            json.Add("success", false);
            switch (errId)
            {
                case 999000:
                    json.Add("errmsg", "系统错误");
                    break;
                case 999001:
                    json.Add("errmsg", "用户名或密码错误");
                    break;
                default:
                    json.Add("errmsg", "未知错误");
                    break;
            }
            return json;
        }
    }
}