﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.Models
{
    public class ManagerListVo
    {
        public int? MgrId { get; set; }

        public string MgrName { get; set; }
        public string RoleName { get; set; }

        public string MgrMobile { get; set; }

        public string MgrRemark { get; set; }

        public DateTime MgrAddtime { get; set; }

        public int MgrStatus { get; set; }
    }
}