﻿
namespace WebSite.Models
{
    public class MoudleListVo
    {
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
        public string ModuleFname { get; set; }
        public string ModuleFicon { get; set; }
        public string ModulePath { get; set; }
        public int Checked { get; set; }
    }
}