﻿using System;

namespace WebSite.Models
{
    public class RoleListVo
    {
        public int RoleId { get; set; }

        public string RoleName { get; set; }

        public string RoleDesc { get; set; }

        public DateTime RoleAddtime { get; set; }
    }
}