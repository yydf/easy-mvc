﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.Models
{
    public class UserVo
    {
        public UserVo(string name)
        {
            this.Name = name;
        }

        public string Name;
    }
}