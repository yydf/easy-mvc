﻿using System;

namespace WebSite.Models
{
    public class RoleVo
    {
        public int? RoleId { get; set; }

        public string RoleName { get; set; }

        public string RoleDesc { get; set; }
    }
}