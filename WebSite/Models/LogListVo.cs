﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.Models
{
    public class LogListVo
    {
        public int LogId { get; set; }

        public string LogText { get; set; }

        public string MgrName { get; set; }

        public string LogIp { get; set; }

        public DateTime LogAddtime { get; set; }
    }
}