﻿using System.Collections.Generic;

namespace WebSite.Models
{
    public class FMoudleListVo
    {
        public string ModuleFname { get; set; }
        public string ModuleFicon { get; set; }
        public List<MoudleListVo> Modules { get; set; }
    }
}