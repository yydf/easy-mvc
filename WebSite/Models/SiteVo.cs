﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.Models
{
    public class SiteVo
    {
        public string SiteName { get; set; }

        public string CopyRight { get; set; }

        public string ICP { get; set; }

        public string StatJS { get; set; }
    }
}