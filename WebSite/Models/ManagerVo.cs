﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.Models
{
    public class ManagerVo
    {
        public int? MgrId { get; set; }
        public string MgrName { get; set; }
        public string MgrMobile { get; set; }
        public string MgrRemark { get; set; }
        public int RoleId { get; set; }
        public int MgrSex { get; set; }
    }
}