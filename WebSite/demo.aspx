﻿<%@ Page Language="C#" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <div>
        <h2>例子</h2>
        <a href="/demo/demo2" target="_blank">返回页面</a><br />
        <a href="/demo/test" target="_blank">返回JSON</a><br />
        <a href="/demo/test2" target="_blank">自定义输出</a>
    </div>
    <div>
        <h2>使用方式</h2>
        <h3>web.config配置</h3>
        <pre>&lt;system.webServer&gt;
    &lt;!-- IIS7以后配置 --&gt;
    &lt;handlers&gt;
      &lt;add name="PageHandlerFactory" verb="*" path="*"
              type="MyMVC.MVCPageHandlerFactory, MyMVC" preCondition="integratedMode"/&gt;
    &lt;/handlers&gt;
&lt;/system.webServer&gt;</pre>
        <h3>Global.asax初始化</h3>
        <pre>void Application_Start(object sender, EventArgs e)
{
    // 运行mvc
    MyMVC.ContextLoader.Startup();
}</pre>
        <h3>Controller实现</h3>
        <pre>public class IndexController : ActionSupport
{
    //返回页面
    [PageUrl("/")]
    public ModelAndView demo()
    {
        return new ModelAndView("demo");
    }

    //返回带内容页面
    [PageUrl("/demo2")]
    public ModelAndView demo2()
    {
        ModelAndView mav = new ModelAndView("demo2");
        mav.addObject("num", 123);
        mav.addObject("user", new UserVo("李四"));
        return mav;
    }

    //返回JSON
    [PageUrl("/test")]
    public Dictionary&lt;string,object&gt; test()
    {
       Dictionary&lt;string, object&gt; dict = new Dictionary&lt;string, object&gt;();
       dict.Add("errcode", 0);
       dict.Add("user", new UserVo("张三"));
       return dict;
    }

    //直接输出
    [PageUrl("/test2")]
    public void test2()
    {
        HttpResponse response = getResponse();
        response.Write("我是直接输出的");
        response.End();
    }
}</pre>
    </div>
</body>
</html>
