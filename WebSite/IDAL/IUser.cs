﻿using MyUtils;

namespace WebSite.IDAL
{
    public interface IUser
    {
        Map checkLogin(string userName, string pwd, string ip);
    }
}
