﻿using System.Collections.Generic;
using WebSite.Entity;
using WebSite.Models;

namespace WebSite.IDAL
{
    public interface IManager
    {
        List<ManagerListVo> getManagerList(string keyWord);

        bool addManager(FaManager mgr);

        bool delManager(string mgrId);

        ManagerVo getManagerVo(string mgrId);

        bool changeFlag(string mgrId, string status);
    }
}
