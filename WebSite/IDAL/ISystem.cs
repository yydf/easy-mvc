﻿using System.Collections.Generic;
using WebSite.Models;

namespace WebSite.IDAL
{
    public interface ISystem
    {
        List<LogListVo> getLogList();

        bool delLog(string logId);

        List<LogListVo> searchLog(string minDate, string maxDate);

        List<FMoudleListVo> getFModuleList();

        List<FMoudleListVo> getFModuleList(string roleId, bool all);

        int getLoginNum(string mgrId);

        LogListVo getLastLog(string mgrId);

        SiteVo getSiteConfig();

        bool editConfig(SiteVo site);
    }
}
