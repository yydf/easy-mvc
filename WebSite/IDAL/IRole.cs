﻿using System.Collections.Generic;
using WebSite.Entity;
using WebSite.Models;

namespace WebSite.IDAL
{
    public interface IRole
    {
        string getRoleName(string roleId);

        List<RoleListVo> getRoleList();

        RoleVo getRoleVo(string roleId);

        bool addRole(FaRole entity, string menuStr);

        bool delRole(string roleId);
    }
}
