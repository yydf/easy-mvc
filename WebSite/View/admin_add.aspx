﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!--#include file="/View/include/meta.aspx" -->
    <title>WebFormMVC测试系统V1.0</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
</head>
<body>
    <article class="page-container">
        <form class="form form-horizontal" id="form-admin-add">
            <input type="hidden" id="MgrId" name="MgrId" value="<%=Context.Items["MgrId"] %>" />
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>管理员：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" class="input-text" value="<%=Context.Items["MgrName"] %>" placeholder="" id="MgrName" name="MgrName" />
                </div>
            </div>
            <%if (Context.Items["MgrId"] == null)
              {%>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>初始密码：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="password" class="input-text" autocomplete="off" value="" placeholder="密码" id="MgrPwd" name="MgrPwd" />
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>确认密码：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="password" class="input-text" autocomplete="off" placeholder="确认新密码" id="MgrPwd2" name="MgrPwd2" />
                </div>
            </div>
            <%} %>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>性别：</label>
                <div class="formControls col-xs-8 col-sm-9 skin-minimal">
                    <div class="radio-box">
                        <input name="MgrSex" type="radio" id="sex-1" value="1" />
                        <label for="sex-1">男</label>
                    </div>
                    <div class="radio-box">
                        <input type="radio" id="sex-2" name="MgrSex" value="0" />
                        <label for="sex-2">女</label>
                    </div>
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>手机：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" class="input-text" value="<%=Context.Items["MgrMobile"] %>" placeholder="" id="MgrMobile" name="MgrMobile" />
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2">角色：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <span class="select-box" style="width: 150px;">
                        <select class="select" name="RoleId" id="RoleId" size="1">
                            <% List<RoleListVo> dataList = (List<RoleListVo>)Context.Items["roleList"];
                               foreach (RoleListVo vo in dataList)
                               { %>
                            <option value="<%=vo.RoleId %>"><%=vo.RoleName %></option>
                            <%} %>
                        </select>
                    </span>
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2">备注：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <textarea name="MgrRemark" id="MgrRemark" cols="" rows="" class="textarea" placeholder="说点什么...100个字符以内" onkeyup="$.Huitextarealength(this,100)"><%=Context.Items["MgrRemark"] %></textarea>
                    <p class="textarea-numberbar"><em class="textarea-length">0</em>/100</p>
                </div>
            </div>
            <div class="row cl">
                <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2">
                    <input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;" />
                </div>
            </div>
        </form>
    </article>

    <!--#include file="/View/include/footer.aspx" -->
    <script type="text/javascript" src="/View/lib/jquery.validation/1.14.0/jquery.validate.js"></script>
    <script type="text/javascript" src="/View/lib/jquery.validation/1.14.0/validate-methods.js"></script>
    <script type="text/javascript" src="/View/lib/jquery.validation/1.14.0/messages_zh.js"></script>

    <script type="text/javascript">
        $(function () {
            $("input[name='MgrSex'][value='<%=Context.Items["MgrSex"] %>']").attr("checked", true);
            $('#RoleId').val('<%=Context.Items["RoleId"] %>');

            $('.skin-minimal input').iCheck({
                checkboxClass: 'icheckbox-blue',
                radioClass: 'iradio-blue',
                increaseArea: '20%'
            });

            $("#form-admin-add").validate({
                rules: {
                    MgrName: {
                        required: true,
                        minlength: 4,
                        maxlength: 16
                    },
                    MgrPwd: {
                        required: true,
                    },
                    MgrPwd2: {
                        required: true,
                        equalTo: "#MgrPwd"
                    },
                    MgrSex: {
                        required: true,
                    },
                    MgrMobile: {
                        required: true,
                        isPhone: true,
                    },
                    RoleId: {
                        required: true,
                    },
                },
                onkeyup: false,
                focusCleanup: true,
                success: "valid",
                submitHandler: function (form) {
                    $(form).ajaxSubmit({
                        type: 'post',
                        url: "addManager",
                        success: function (data) {
                            if (data.success) {
                                layer.msg('添加成功!', { icon: 1, time: 1000 }, function () {
                                    var index = parent.layer.getFrameIndex(window.name);
                                    parent.$('#btn-refresh').click();
                                    parent.layer.close(index);
                                });
                            } else {
                                layer.msg(data.errmsg, { icon: 1, time: 3000 });
                            }
                        },
                        error: function (XmlHttpRequest, textStatus, errorThrown) {
                            layer.msg('error!', { icon: 1, time: 1000 });
                        }
                    });
                }
            });
        });
    </script>
</body>
</html>
