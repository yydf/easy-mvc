﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!--#include file="/View/include/meta.aspx" -->
    <title>WebFormMVC测试系统V1.0</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
</head>
<body>
    <div class="page-container">
        <p class="f-20 text-success">欢迎使用<%=Context.Items["SiteName"] %></p>
        <p>登录次数：<%=Context.Items["loginNum"] %> </p>
        <%object vo = Context.Items["logVo"]; if (vo == null)
          {%>
        <p>上次登录IP：暂无  上次登录时间：暂无</p>
        <%}
          else
          { %>
        <p>上次登录IP：<%=((LogListVo)vo).LogIp %>  上次登录时间：<%=((LogListVo)vo).LogAddtime %></p>
        <%} %>
        <table class="table table-border table-bordered table-bg mt-20">
            <thead>
                <tr>
                    <th colspan="2" scope="col">服务器信息</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td width="30%">服务器计算机名</td>
                    <td><%=Context.Items["server"] %></td>
                </tr>
                <tr>
                    <td>.NET Framework 版本 </td>
                    <td><%=Context.Items["version"] %></td>
                </tr>
                <tr>
                    <td>服务器当前时间 </td>
                    <td><%=Context.Items["time"] %></td>
                </tr>
                <tr>
                    <td>当前SessionID </td>
                    <td><%=Context.Items["sessionId"] %></td>
                </tr>
            </tbody>
        </table>
    </div>
    <footer class="footer mt-20">
        <div class="container">
            <p>
                <%=Context.Items["CopyRight"] %>
                <br />
                <%=Context.Items["ICP"] %>
            </p>
        </div>
    </footer>

    <!--#include file="/View/include/footer.aspx" -->
</body>
</html>
