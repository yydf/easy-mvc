﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!--#include file="/View/include/meta.aspx" -->
    <title><%=Context.Items["SiteName"] %>V1.0</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
</head>
<body>
    <header class="navbar-wrapper">
        <div class="navbar navbar-fixed-top">
            <div class="container-fluid cl">
                <a class="logo navbar-logo f-l mr-10 hidden-xs" href="/toMain"><%=Context.Items["SiteName"] %></a>
                <span class="logo navbar-slogan f-l mr-10 hidden-xs">v1.0</span>
                <a aria-hidden="false" class="nav-toggle Hui-iconfont visible-xs" href="javascript:;">&#xe667;</a>
                <nav id="Hui-userbar" class="nav navbar-nav navbar-userbar hidden-xs">
                    <ul class="cl">
                        <li><%=Context.Items["RoleName"] %></li>
                        <li class="dropDown dropDown_hover">
                            <a href="#" class="dropDown_A"><%=Context.Items["MgrName"] %> <i class="Hui-iconfont">&#xe6d5;</i></a>
                            <ul class="dropDown-menu menu radius box-shadow">
                                <li><a href="javascript:;" onclick="myselfinfo()">个人信息</a></li>
                                <li><a href="javascript:;" onclick="exit('您确定要切换账户吗？')">切换账户</a></li>
                                <li><a href="javascript:;" onclick="exit('您确定要退出系统吗?')">退出</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <aside class="Hui-aside">
        <div class="menu_dropdown bk_2">
            <% List<FMoudleListVo> dataList = (List<FMoudleListVo>)Context.Items["moduleList"];
               foreach (FMoudleListVo vo in dataList)
               { %>
            <dl id="menu-admin">
                <dt><i class="Hui-iconfont"><%=vo.ModuleFicon %></i>&nbsp;<%=vo.ModuleFname %><i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
                <dd>
                    <ul>
                        <%foreach (MoudleListVo vo2 in vo.Modules)
                          { %>
                        <li><a data-href="<%=vo2.ModulePath %>" data-title="<%=vo2.ModuleName %>" href="javascript:void(0)"><%=vo2.ModuleName %></a></li>
                        <%} %>
                    </ul>
                </dd>
            </dl>
            <%} %>
        </div>
    </aside>
    <div class="dislpayArrow hidden-xs"><a class="pngfix" href="javascript:void(0);" onclick="displaynavbar(this)"></a></div>
    <section class="Hui-article-box">
        <div id="Hui-tabNav" class="Hui-tabNav hidden-xs">
            <div class="Hui-tabNav-wp">
                <ul id="min_title_list" class="acrossTab cl">
                    <li class="active">
                        <span title="欢迎页" data-href="welcome.html">欢迎页</span>
                        <em></em></li>
                </ul>
            </div>
            <div class="Hui-tabNav-more btn-group"><a id="js-tabNav-prev" class="btn radius btn-default size-S" href="javascript:;"><i class="Hui-iconfont">&#xe6d4;</i></a><a id="js-tabNav-next" class="btn radius btn-default size-S" href="javascript:;"><i class="Hui-iconfont">&#xe6d7;</i></a></div>
        </div>
        <div id="iframe_box" class="Hui-article">
            <div class="show_iframe">
                <div style="display: none" class="loading"></div>
                <iframe scrolling="yes" frameborder="0" src="/toWelcome"></iframe>
            </div>
        </div>
    </section>

    <div class="contextMenu" id="Huiadminmenu">
        <ul>
            <li id="closethis">关闭当前 </li>
            <li id="closeall">关闭全部 </li>
        </ul>
    </div>

    <!--#include file="/View/include/footer.aspx" -->

    <!--请在下方写此页面业务相关的脚本-->
    <script type="text/javascript" src="/View/lib/jquery.contextmenu/jquery.contextmenu.r2.js"></script>
    <script type="text/javascript">
        /*个人信息*/
        function myselfinfo() {
            layer.open({
                type: 1,
                area: ['300px', '200px'],
                fix: false, //不固定
                maxmin: true,
                shade: 0.4,
                title: '查看信息',
                content: '<div>管理员信息</div>'
            });
        }

        function exit(msg) {
            layer.confirm(msg, {
                btn: ['确定', '取消']
            }, function (index) {
                location.href = "/user/logout";
            }, function (index) {
                layer.close(index);
            });
        }
    </script>
</body>
</html>
