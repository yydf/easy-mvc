﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!--#include file="/View/include/meta.aspx" -->
    <title>WebFormMVC测试系统V1.0</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
</head>
<body>
    <article class="page-container">
        <form class="form form-horizontal" id="form-admin-add">
            <input type="hidden" id="RoleId" name="RoleId" value="<%=Context.Items["RoleId"] %>" />
            <input type="hidden" id="menuStr" name="menuStr" />
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>角色名称：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" class="input-text" value="<%=Context.Items["RoleName"] %>" placeholder="" id="RoleName" name="RoleName" />
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2">角色描述：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <textarea name="RoleDesc" id="RoleDesc" cols="" rows="" class="textarea" placeholder="说点什么...100个字符以内" onkeyup="$.Huitextarealength(this,100)"><%=Context.Items["RoleDesc"] %></textarea>
                    <p class="textarea-numberbar"><em class="textarea-length">0</em>/100</p>
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-2">管理模块：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <% List<FMoudleListVo> dataList = (List<FMoudleListVo>)Context.Items["moduleList"];
                       foreach (FMoudleListVo vo in dataList)
                       { %>
                    <dl class="permission-list">
                        <dt>
                            <label>
                                <input type="checkbox" class="firstMenu" />
                                <%=vo.ModuleFname %></label>
                        </dt>
                        <dd>
                            <dl class="cl permission-list2">
                                <% foreach (MoudleListVo vo2 in vo.Modules)
                                   {%>
                                <dt>
                                    <%if(vo2.Checked == 0){ %>
                                    <label>
                                        <input type="checkbox" value="<%=vo2.ModuleId %>"
                                            class="secondMenu" /><%=vo2.ModuleName %></label>
                                    <%}else{ %>
                                    <label>
                                        <input type="checkbox" value="<%=vo2.ModuleId %>"
                                            class="secondMenu" checked="checked" /><%=vo2.ModuleName %></label>
                                    <%} %>
                                </dt>
                                <%} %>
                            </dl>
                        </dd>
                    </dl>
                    <%} %>
                </div>
            </div>
            <div class="row cl">
                <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2">
                    <input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;" />
                </div>
            </div>
        </form>
    </article>

    <!--#include file="/View/include/footer.aspx" -->
    <script type="text/javascript" src="/View/lib/jquery.validation/1.14.0/jquery.validate.js"></script>
    <script type="text/javascript" src="/View/lib/jquery.validation/1.14.0/validate-methods.js"></script>
    <script type="text/javascript" src="/View/lib/jquery.validation/1.14.0/messages_zh.js"></script>

    <script type="text/javascript">
        $(function () {
            $(".firstMenu").click(function () {
                $(this).closest("dt").next().find(".secondMenu").prop(
                        "checked", $(this).prop("checked"));
            });
            $(".secondMenu").click(function () {
                if (!$(this)[0].hasAttribute("checked"))
                    $(this).closest("dd").prev().find(".firstMenu")
                            .prop("checked", false);
            });

            $("#form-admin-add").validate({
                rules: {
                    RoleName: {
                        required: true,
                        minlength: 4,
                        maxlength: 16
                    }
                },
                onkeyup: false,
                focusCleanup: true,
                success: "valid",
                submitHandler: function (form) {
                    var menuStr = [];
                    $("input:checked").each(function () {
                        if ($(this).val() != 'on') {
                            menuStr[menuStr.length] = $(this).val();
                        }
                    });
                    $('#menuStr').val(menuStr.join(','));
                    $(form).ajaxSubmit({
                        type: 'post',
                        url: "addRole",
                        success: function (data) {
                            if (data.success) {
                                layer.msg('添加成功!', { icon: 1, time: 1000 }, function () {
                                    var index = parent.layer.getFrameIndex(window.name);
                                    parent.$('#btn-refresh').click();
                                    parent.layer.close(index);
                                });
                            } else {
                                layer.msg(data.errmsg, { icon: 1, time: 3000 });
                            }
                        },
                        error: function (XmlHttpRequest, textStatus, errorThrown) {
                            layer.msg('error!', { icon: 1, time: 1000 });
                        }
                    });
                }
            });
        });
    </script>
</body>
</html>
