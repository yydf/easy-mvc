﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!--#include file="/View/include/meta.aspx" -->
    <title>WebFormMVC测试系统V1.0</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
</head>
<body>
    <nav class="breadcrumb">
        <i class="Hui-iconfont">&#xe67f;</i> 首页
	<span class="c-gray en">&gt;</span>
        系统管理
	<span class="c-gray en">&gt;</span>
        系统日志
	<a class="btn btn-success radius r" id="btn-refresh" style="line-height: 1.6em; margin-top: 3px" onclick="javascript:location.replace(location.href);" href="javascript:;" title="刷新"><i class="Hui-iconfont">&#xe68f;</i></a>
    </nav>
    <div class="page-container">
        <div class="text-c">
            日期范围：<input type="text" onfocus="WdatePicker({ maxDate:'#F{$dp.$D(\'logmax\')||\'%y-%M-%d\'}' })" id="logmin" value="<%=Context.Items["logmin"] %>" class="input-text Wdate" style="width: 120px;" />
            <input type="text" onfocus="WdatePicker({ minDate:'#F{$dp.$D(\'logmin\')}',maxDate:'%y-%M-%d' })" id="logmax" value="<%=Context.Items["logmax"] %>" class="input-text Wdate" style="width: 120px;" />
            <button name="" id="Button1" class="btn btn-success" type="submit" onclick="search('searchLog', 'a='+$('#logmin').val()+'&b='+$('#logmax').val());"><i class="Hui-iconfont">&#xe665;</i> 搜日志</button>
        </div>
        <div class="cl pd-5 bg-1 bk-gray mt-10">
            <span class="r">共有数据：<strong id="list_size">1</strong> 条</span>
        </div>
        <table class="table table-border table-bordered table-bg table-hover table-sort">
            <thead>
                <tr class="text-c">
                    <th width="40">ID</th>
                    <th>内容</th>
                    <th width="17%">用户名</th>
                    <th width="120">客户端IP</th>
                    <th width="120">时间</th>
                    <th width="70">操作</th>
                </tr>
            </thead>
            <tbody>
                <% List<LogListVo> dataList = (List<LogListVo>)Context.Items["dataList"];
                   foreach (LogListVo vo in dataList)
                   { %>
                <tr class="text-c">
                    <td><%=vo.LogId %></td>
                    <td><%=vo.LogText %></td>
                    <td><%=vo.MgrName %></td>
                    <td><%=vo.LogIp %></td>
                    <td><%=vo.LogAddtime %></td>
                    <td>
                        <a title="删除" href="javascript:;" onclick="del(this, 'delLog','<%=vo.LogId %>')" class="ml-5" style="text-decoration: none"><i class="Hui-iconfont">&#xe6e2;</i></a></td>
                </tr>
                <%} %>
            </tbody>
        </table>
        <div id="pageNav" class="pageNav"></div>
    </div>

    <!--#include file="/View/include/footer.aspx" -->

    <script src="/View/lib/My97DatePicker/4.8/WdatePicker.js"></script>
</body>
</html>
