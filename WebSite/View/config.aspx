﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!--#include file="/View/include/meta.aspx" -->
    <title>WebFormMVC测试系统V1.0</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
</head>
<body>
    <article class="page-container">
        <form method="post" class="form form-horizontal"
            id="form-add">
            <div id="tab-system" class="HuiTab">
                <div class="tabBar cl">
                    <span>基本设置</span> <span>安全设置</span>
                </div>
                <div class="tabCon">
                    <div class="row cl">
                        <label class="form-label col-xs-4 col-sm-2">
                            <span
                                class="c-red">*</span> 网站名称：
                        </label>
                        <div class="formControls col-xs-8 col-sm-8">
                            <input type="text" id="SiteName" name="SiteName" placeholder="控制在25个字、50个字节以内"
                                class="input-text" value="<%=Context.Items["SiteName"] %>" />
                        </div>
                    </div>
                    <%--<div class="row cl">
                        <label class="form-label col-xs-4 col-sm-2">
                            “常用功能”菜单：
                        </label>
                        <div class="formControls col-xs-8 col-sm-9 skin-minimal">
                            <div class="radio-box">
                                <input name="isShowUsedFuction" id="isShowUsedFuction1" type="radio" value="1" />
                                <label for='isShowUsedFuction1'>启用</label>
                            </div>
                            <div class="radio-box">
                                <input type="radio" id="isShowUsedFuction0" name="isShowUsedFuction" value="0" />
                                <label for='isShowUsedFuction0'>停用</label>
                            </div>
                        </div>
                    </div>--%>
                    <div class="row cl">
                        <label class="form-label col-xs-4 col-sm-2">底部版权信息： </label>
                        <div class="formControls col-xs-8 col-sm-8">
                            <input type="text" id="CopyRight"
                                placeholder="&copy; 2016 H-ui.net" name="CopyRight" class="input-text" value="<%=Context.Items["CopyRight"] %>" />
                        </div>
                    </div>
                    <div class="row cl">
                        <label class="form-label col-xs-4 col-sm-2">备案号：</label>
                        <div class="formControls col-xs-8 col-sm-8">
                            <input type="text" id="ICP" name="ICP" placeholder="京ICP备00000000号" class="input-text" value="<%=Context.Items["ICP"] %>" />
                        </div>
                    </div>
                    <div class="row cl">
                        <label class="form-label col-xs-4 col-sm-2">统计代码：</label>
                        <div class="formControls col-xs-8 col-sm-8">
                            <textarea class="textarea" id="StatJS" name="StatJS"><%=Context.Items["StatJS"] %></textarea>
                        </div>
                    </div>
                </div>
                <div class="tabCon">
                    <div class="row cl">
                        <label class="form-label col-xs-4 col-sm-2">允许访问后台的IP列表：</label>
                        <div class="formControls col-xs-8 col-sm-8">
                            <textarea class="textarea" placeholder="不同IP间以“|”分隔"
                                name="allowIP" id="allowIP"></textarea>
                        </div>
                    </div>
                    <div class="row cl">
                        <label class="form-label col-xs-4 col-sm-2">后台登录失败最大次数：</label>
                        <div class="formControls col-xs-8 col-sm-8">
                            <input type="text" class="input-text" id="loginFaildNum" name="loginFaildNum" />
                        </div>
                    </div>
                </div>
                <div class="row cl">
                    <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2">
                        <button class="btn btn-primary radius" type="submit">
                            <i class="Hui-iconfont">&#xe632;</i> 保存
                        </button>
                    </div>
                </div>
            </div>

        </form>
    </article>

    <!--#include file="/View/include/footer.aspx" -->
    <script type="text/javascript" src="/View/lib/jquery.validation/1.14.0/jquery.validate.js"></script>
    <script type="text/javascript" src="/View/lib/jquery.validation/1.14.0/validate-methods.js"></script>
    <script type="text/javascript" src="/View/lib/jquery.validation/1.14.0/messages_zh.js"></script>

    <script type="text/javascript">
        $(function () {
            $("#tab-system").Huitab({
                index: 0
            });

            //$('.skin-minimal input').iCheck({
            //    checkboxClass: 'icheckbox-blue',
            //    radioClass: 'iradio-blue',
            //    increaseArea: '20%'
            //});

            $("#form-add").validate({
                rules: {
                    SiteName: {
                        required: true,
                        maxlength: 25
                    }
                },
                onkeyup: false,
                focusCleanup: true,
                success: "valid",
                submitHandler: function (form) {
                    $(form).ajaxSubmit({
                        type: 'post',
                        url: "editConfig",
                        success: function (data) {
                            if (data.success) {
                                layer.msg('保存成功!', { icon: 1, time: 1000 }, function () {
                                    var index = parent.layer.getFrameIndex(window.name);
                                    //parent.$('#btn-refresh').click();
                                    parent.layer.close(index);
                                });
                            } else {
                                layer.msg(data.errmsg, { icon: 1, time: 3000 });
                            }
                        },
                        error: function (XmlHttpRequest, textStatus, errorThrown) {
                            layer.msg('error!', { icon: 1, time: 1000 });
                        }
                    });
                }
            });
        });
    </script>
</body>
</html>
