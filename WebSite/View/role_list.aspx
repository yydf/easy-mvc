﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!--#include file="/View/include/meta.aspx" -->
    <title>WebFormMVC测试系统V1.0</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
</head>
<body>
    <nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 角色管理 <span class="c-gray en">&gt;</span> 角色列表 <a class="btn btn-success radius r" id="btn-refresh" style="line-height: 1.6em; margin-top: 3px" href="javascript:;" onclick="javascript:location.replace(location.href);" title="刷新"><i class="Hui-iconfont">&#xe68f;</i></a></nav>
    <div class="page-container">
        <div class="cl pd-5 bg-1 bk-gray"><span class="l"><a href="javascript:;" onclick="add('添加角色','toAddRole','800','550')" class="btn btn-primary radius"><i class="Hui-iconfont">&#xe600;</i> 添加角色</a></span> <span class="r">共有数据：<strong id="list_size"><%=Context.Items["size"] %></strong> 条</span> </div>
        <table class="table table-border table-bordered table-bg">
            <thead>
                <tr>
                    <th scope="col" colspan="9">角色列表</th>
                </tr>
                <tr class="text-c">
                    <th width="40">ID</th>
                    <th width="150">角色名</th>
                    <th>描述</th>
                    <th width="130">创建时间</th>
                    <th width="100">操作</th>
                </tr>
            </thead>
            <tbody>
                <% List<RoleListVo> dataList = (List<RoleListVo>)Context.Items["dataList"];
                   foreach (RoleListVo vo in dataList)
                   { %>
                <tr class="text-c">
                    <td><%=vo.RoleId %></td>
                    <td><%=vo.RoleName %></td>
                    <td><%=vo.RoleDesc %></td>
                    <td><%=vo.RoleAddtime %></td>
                    <%if (vo.RoleId == 1)
                      {%>
                    <td><a title="编辑" href="javascript:;" onclick="show('该角色不能编辑')" class="ml-5" style="text-decoration: none"><i class="Hui-iconfont">&#xe6df;</i></a> <a title="删除" href="javascript:;" onclick="show('该角色不能删除')" class="ml-5" style="text-decoration: none"><i class="Hui-iconfont">&#xe6e2;</i></a></td>
                    <%}
                      else
                      { %>
                    <td><a title="编辑" href="javascript:;" onclick="edit('角色编辑','toAddRole','<%=vo.RoleId %>','800','500')" class="ml-5" style="text-decoration: none"><i class="Hui-iconfont">&#xe6df;</i></a> <a title="删除" href="javascript:;" onclick="del(this,'delRole','<%=vo.RoleId %>')" class="ml-5" style="text-decoration: none"><i class="Hui-iconfont">&#xe6e2;</i></a></td>
                    <%} %>
                </tr>
                <%  } %>
            </tbody>
        </table>
    </div>

    <!--#include file="/View/include/footer.aspx" -->
</body>
</html>
