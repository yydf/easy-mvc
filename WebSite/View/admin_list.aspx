﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!--#include file="/View/include/meta.aspx" -->
    <title>WebFormMVC测试系统V1.0</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
</head>
<body>
    <nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 管理员管理 <span class="c-gray en">&gt;</span> 管理员列表 <a class="btn btn-success radius r" id="btn-refresh" style="line-height: 1.6em; margin-top: 3px" href="javascript:;" onclick="javascript:location.replace(location.href);" title="刷新"><i class="Hui-iconfont">&#xe68f;</i></a></nav>
    <div class="page-container">
        <div class="text-l">
            <input type="text" class="input-text" style="width: 250px" value="<%=Context.Items["keyWord"] %>" placeholder="输入管理员名称" id="keyWord" name="keyWord" />
            <button type="submit" class="btn btn-success" id="Button1" name="" onclick="search('toManagerList', 'a=' + $('#keyWord').val());"><i class="Hui-iconfont">&#xe665;</i> 搜用户</button>
        </div>
        <div class="cl pd-5 bg-1 bk-gray mt-20"><span class="l"><a href="javascript:;" onclick="add('添加管理员','toAddManager','800','550')" class="btn btn-primary radius"><i class="Hui-iconfont">&#xe600;</i> 添加管理员</a></span> <span class="r">共有数据：<strong id="list_size"><%=Context.Items["size"] %></strong> 条</span> </div>
        <table class="table table-border table-bordered table-bg">
            <thead>
                <tr>
                    <th scope="col" colspan="9">员工列表</th>
                </tr>
                <tr class="text-c">
                    <th width="40">ID</th>
                    <th width="150">登录名</th>
                    <th width="110">手机</th>
                    <th width="120">角色</th>
                    <th>备注</th>
                    <th width="130">加入时间</th>
                    <th width="100">是否已启用</th>
                    <th width="100">操作</th>
                </tr>
            </thead>
            <tbody>
                <% List<ManagerListVo> dataList = (List<ManagerListVo>)Context.Items["dataList"];
                   foreach (ManagerListVo vo in dataList)
                   { %>
                <tr class="text-c">
                    <td><%=vo.MgrId %></td>
                    <td><%=vo.MgrName %></td>
                    <td><%=vo.MgrMobile %></td>
                    <td><%=vo.RoleName %></td>
                    <td><%=vo.MgrRemark %></td>
                    <td><%=vo.MgrAddtime %></td>
                    <%if (vo.MgrStatus == 1)
                      {%><td class="td-status"><span class="label label-success radius">已启用</span></td>
                    <td class="td-manage"><a style="text-decoration: none" onclick="status('确定要停用该管理员吗？','changeFlag?a=<%=vo.MgrId %>&s=0','已停用')" href="javascript:;" title="停用"><i class="Hui-iconfont">&#xe631;</i></a> <a title="编辑" href="javascript:;" onclick="edit('管理员编辑','toAddManager','<%=vo.MgrId %>','800','500')" class="ml-5" style="text-decoration: none"><i class="Hui-iconfont">&#xe6df;</i></a> <a title="删除" href="javascript:;" onclick="del(this,'delManager','<%=vo.MgrId %>')" class="ml-5" style="text-decoration: none"><i class="Hui-iconfont">&#xe6e2;</i></a></td>
                    <%}
                      else
                      { %><td class="td-status"><span class="label radius">已停用</span></td>
                    <td class="td-manage"><a style="text-decoration: none" onclick="status('确定要启用该管理员吗？','changeFlag?a=<%=vo.MgrId %>&s=1','已启用')" href="javascript:;" title="启用"><i class="Hui-iconfont">&#xe615;</i></a> <a title="编辑" href="javascript:;" onclick="edit('管理员编辑','toAddManager','<%=vo.MgrId %>','800','500')" class="ml-5" style="text-decoration: none"><i class="Hui-iconfont">&#xe6df;</i></a> <a title="删除" href="javascript:;" onclick="del(this,'delManager','<%=vo.MgrId %>')" class="ml-5" style="text-decoration: none"><i class="Hui-iconfont">&#xe6e2;</i></a></td>
                    <%} %>
                </tr>
                <%  } %>
            </tbody>
        </table>
    </div>

    <!--#include file="/View/include/footer.aspx" -->
</body>
</html>
