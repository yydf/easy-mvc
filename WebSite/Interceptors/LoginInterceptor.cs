﻿using MyMVC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace WebSite.Interceptors
{
    public class LoginInterceptor : AbstractActionInterceptor
    {
        public override bool intercept(HttpSessionState session, HttpRequest arg0, HttpResponse arg1)
        {
            if (arg0.Path == "/" || arg0.Path.Contains("/user/toLogin"))
                return true;
            if (session == null || session["mgrId"] == null)
            {
                arg1.Redirect("~/", true);
                return false;
            }
            return true;
        }
    }
}