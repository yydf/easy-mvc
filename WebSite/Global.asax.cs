﻿using System;
using System.Web;

namespace WebSite
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // 运行mvc
            MyMVC.ContextLoader.Startup();
        }

        void Application_End(object sender, EventArgs e)
        {
            //  在应用程序关闭时运行的代码

        }

        void Application_Error(object sender, EventArgs e)
        {
            // 在出现未处理的错误时运行的代码

        }

        void Application_BeginRequest(object sender, EventArgs e)
        {
        }
    }
}
